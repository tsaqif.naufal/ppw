$(document).ready(function(){
	$('#accordion').accordion({
		active: false,
		collapsible: true,
		heightStyle: "content"          
	});
});

var darkTheme = true;

function changeTheme() {
    if(darkTheme) {
        $("body").css({"background-color": "#E4EBE9", "color": "#121212"});
        darkTheme = false;
    }  
    else {
        $("body").css({"background-color": "#121212", "color": "#FFFFFF"});
        darkTheme = true;
    }
}