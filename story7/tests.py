from django.test import TestCase
from django.urls import resolve
from .views import index
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class Story7UnitTest(TestCase):
	def test_url_index_exists(self):
		response = self.client.get('/')
		self.assertEqual(response.status_code, 200)

	def test_using_index_function(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_using_landing_page_template(self):
		response = self.client.get('/')
		self.assertTemplateUsed(response, 'story7/index.html')

	def test_title_in_template(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('PROFILE', html_response)

	def test_self_image_in_template(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('<img', html_response)

	def test_name_in_template(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('Tsaqif Naufal', html_response)

	def test_self_description_in_template(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('Like Rancho said, Aal Izz Well!', html_response)

	def test_change_theme_button_in_template(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('Ubah Tema</button>', html_response)

	def test_accordion_in_template(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('<div id="accordion">', html_response)

	def test_accordion_first_section_in_template(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('Aktivitas</h3>', html_response)

	def test_accordion_second_section_in_template(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('Organisasi dan Kepanitiaan</h3>', html_response)

	def test_accordion_third_section_in_template(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('Prestasi</h3>', html_response)

	def test_button_has_onclick_attribute(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('<button onclick="changeTheme()"', html_response)

	def test_jquery_script_in_template(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('<script src="/static/story7/js/jquery-3.4.1.min.js"', html_response)

	def test_other_js_script_in_template(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('<script src="/static/story7/js/story7.js"', html_response)


class Story7FunctionalTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')

		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Story7FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Story7FunctionalTest, self).tearDown()

	def test_input_status(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000')

		button = selenium.find_element_by_class_name('btn')
		header1 = selenium.find_element_by_id('header-1')
		header2 = selenium.find_element_by_id('header-2')
		header3 = selenium.find_element_by_id('header-3')
		time.sleep(5)

		header1.click()
		time.sleep(3)
		
		header2.click()
		time.sleep(3)
		
		header3.click()
		time.sleep(5)
		
		button.click()
		time.sleep(5)

		header1.click()
		time.sleep(3)
		
		header2.click()
		time.sleep(3)
		
		header3.click()
		time.sleep(5)